
import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Image, ImageBackground, ScrollView } from 'react-native';
import logo from './assets/sem_gluten.svg';
export default function GrupoEAssociacoes({ navigation }) {
  return (
    <View style={[styles.container, { flexDirection: "column" }]}>
      <ImageBackground source={require('./assets/sem_gluten.png')} resizeMode="cover" style={styles.backgroundImage}>
      </ImageBackground>
      <View style={{ flex: 5, width: '100%', flexDirection: 'column', alignItems: 'center' }} >
        <ScrollView>
          <View >
          <Image style={styles.img} source={require('./assets/page_grupoeassociacoes.png')} />
          </View>
          <Text style={styles.texttitulo}>{"\n"}Instituições/Associações ligadas à Doença Celíaca{"\n"}</Text>
          <Text style={styles.texttitulo}>{"\n"}ACELBRA (Associação de Celíacos do Brasil){"\n"}</Text>
          <Text style={styles.texttitulo}>{"\n"}FENACELBRA (Federação Nacional das Associações de Celíacos do Brasil){"\n"}</Text>
          <Text style={styles.text}>{"\n"}Foi fundada em 2006, atua como porta voz e dá suporte às associações e grupos de celíacos de todo o país junto a instituições governamentais e privadas. Trata-se de uma organização civil, sem fins econômicos, que prega uma mensagem dirigida àqueles que ainda não sabem e/ou não querem “reconhecer” a doença celíaca.</Text>
          </ScrollView>
      </View>
      <StatusBar style="auto" />
    </View >

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',
    alignItems: 'center',
    height: '100%',
    width: '100%'
    // justifyContent: 'center',
  },
  img: {
    alignItems: 'center',
    justifyContent: 'center',
    width: '110%',
    paddingRight: '15%'
  },
  subtitulo: {
    color: '#fff'
  },
  text: {
    color: '#fff',
    paddingLeft: '5%'
  },
  texttitulo: {
    color: '#b2daaa',
    fontWeight: 'bold',
    paddingLeft: '5%'
  },
  header: {
    color: '#fff',
    fontSize: 40,
    fontFamily: 'sans-serif-medium',
    fontWeight: 'bold',
    backgroundColor: 'rgba(52, 52, 52, 0)'
  },
  backgroundImage: {
    flex: 2,
    alignItems: 'center',
    width: '100%'
  }
});
