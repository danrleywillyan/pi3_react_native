
import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Image, ImageBackground, ScrollView } from 'react-native';
import logo from './assets/sem_gluten.svg';
export default function Diagnosticos({ navigation }) {
  return (
    <View style={[styles.container, { flexDirection: "column" }]}>
      <ImageBackground source={require('./assets/sem_gluten.png')} resizeMode="cover" style={styles.backgroundImage}>
      </ImageBackground>
      <View style={{ flex: 5, width: '100%', flexDirection: 'column', alignItems: 'center' }} >
        <ScrollView>
          <View >
            <Image style={styles.img} source={require('./assets/page_diagnostico.png')} />
          </View>
          <Text style={styles.text}>{"\n"}Com sintomas parecidos a diversos outros problemas gastrointestinais, não é fácil ter certeza de que o glúten é o responsável pelo incômodo. O diagnóstico é feito por exame clínico com médico especialista (gastroenterologista), que vai analisar os sintomas para confirmar a detecção.</Text>
          <Text style={styles.texttitulo}>{"\n"}Análise Clínica</Text>
          <Text style={styles.text}>{"\n"}O profissional da saúde verifica a recorrência dos sintomas reclamados pelo paciente.</Text>
          <Text style={styles.texttitulo}>{"\n"}Análise Laboratorial</Text>
          <Text style={styles.text}>{"\n\n"} Seguindo o protocolo clínico do SUS, o médico poderá solicitar um exame de sangue para verificar a dosagem de Imunoglobina IgA e anticorpo Antitrasglutaminase IgA e IgG. Se der positivo, o paciente é encaminhado para a biópsia.</Text>
          <Text style={styles.texttitulo}>{"\n"}Análise histopatológica</Text>
          <Text style={styles.text}>{"\n\n"}É realizada uma endoscopia com biópsia do bulbo e intestino delgado. No paciente celíaco a mucosa do intestino apresenta vilosidade achatada (atrofiadas).</Text>
          <Text style={styles.texttitulo}>{"\n"}Análise genética</Text>
          <Text style={styles.text}>{"\n"}O teste é bastante útil para descartar a doença celíaca. Estudos recentes demonstram que a presença dos marcadores DQ2 e DQ8 compondo o antígeno HLA estão associados a doença celíaca. Contudo, 15% a 30% da população geral também apresentam estes marcadores, apesar de não terem a doença. O exame então serve para excluir a possibilidade de doença celíaca, pois se o paciente não apresentar os marcadores DQ2 e DQ8 há 99% de chance de ter um diagnóstico negativo.</Text>
        </ScrollView>
      </View>
      <StatusBar style="auto" />
    </View >

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',
    alignItems: 'center',
    height: '100%',
    width: '100%'
    // justifyContent: 'center',
  },
  img: {
    alignItems: 'center',
    justifyContent: 'center',
    width: '120%',
    paddingRight: '15%'
  },
  subtitulo: {
    color: '#fff'
  },
  text: {
    color: '#fff',
    paddingLeft: '5%'
  },
  texttitulo: {
    color: '#b2daaa',
    fontWeight: 'bold',
    paddingLeft: '5%'
  },
  header: {
    color: '#fff',
    fontSize: 40,
    fontFamily: 'sans-serif-medium',
    fontWeight: 'bold',
    backgroundColor: 'rgba(52, 52, 52, 0)'
  },
  backgroundImage: {
    flex: 2,
    alignItems: 'center',
    width: '100%'
  }
});