
import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Image, ImageBackground, ScrollView } from 'react-native';
import logo from './assets/sem_gluten.svg';
export default function tratamento({ navigation }) {
  return (
    <View style={[styles.container, { flexDirection: "column" }]}>
      <ImageBackground source={require('./assets/sem_gluten.png')} resizeMode="cover" style={styles.backgroundImage}>
      </ImageBackground>
      <View style={{ flex: 5, width: '100%', flexDirection: 'column', alignItems: 'center' }} >
        <ScrollView>
          <View >
          <Image style={styles.img} source={require('./assets/page_tratamento.png')} />
          </View>
          <Text style={styles.text}>{"\n"}Não existem medicamentos ou procedimentos específicos para tratar a doença celíaca. A única maneira de se livrar dos transtornos intestinais e evitar complicações é eliminar todos os produtos com glúten. O principal tratamento é a dieta com total ausência de glúten, quando a proteína é excluída da alimentação os sintomas desaparecem.{"\n"}
            Um nutricionista ajuda a tirar dúvidas sobre a dieta restritiva e orienta como substituir os itens que não podem entrar na dieta.</Text>
          <Text style={styles.texttitulo}>{"\n"}Alimentos que deverão ser evitados:</Text>
          <Text style={styles.text}>{"\n"}
          •	Pão{"\n"}
          •	Macarrão{"\n"}
          •	Pizza {"\n"}
          •	Pastel {"\n"}
          •	Molhos prontos {"\n"}
          •	Sopas instantâneas {"\n"}
          •	Achocolatados {"\n"}
          •	Cerveja {"\n"}
          •	Bolos {"\n"}
          •	Biscoitos {"\n"}
          •	Uísque {"\n"}
          •	Vodka{"\n"}
          •	Cevada{"\n"}
          •	Aveia{"\n"}
          •	Trigo{"\n"}
          •	Quibe{"\n"}
          •	Salsicha{"\n"}
          •	Temperos comerciais{"\n"}
          •	Centeio{"\n"}
          •	Almôndega{"\n"}
          •	Produtos dietéticos{"\n"}
          •	Misturas com malte{"\n"}
          •	Flocos cereais{"\n"}
          •	Leite com sabor{"\n"}</Text>

          <Text style={styles.texttitulo}>{"\n"}Alimentos permitidos</Text>
          <Text style={styles.text}>{"\n"}
          •	Cereais: milho, arroz{"\n"}
          •	Farinhas: arroz, mandioca, milho, fubá, fécula de batata, fécula de mandioca, polvilho doce, polvilho azedo{"\n"}
          •	Gorduras: gordura vegetal, óleos, margarinas{"\n"}
          •	Laticínios: leite, manteiga, queijos, derivados{"\n"}
          •	Carnes e ovos: aves, suínos, bovinos, caprinos, miúdos, peixes, frutos do mar{"\n"}
          •	Hortaliças e leguminosas: folhosas, legumes, tubérculos (feijão, cará, inhame, soja, grão de bico, ervilha, lentilha, batata, mandioca){"\n"}
          •	Frutas: todas, ao natural e sucos.{"\n"}
          Observação: Nem todo paciente celíaco tem o mesmo grau da doença.Há casos sensíveis em que apenas 50 miligramas da proteína - o equivalente a um centésimo de uma fatia de pão - já lesionam as paredes do intestino.Em outros casos uma pequena quantidade de glúten é tolerada.{"\n"}{"\n"}
          <Text style={styles.texttitulo}>{"\n"}Contaminação cruzada{"\n"}{"\n"}</Text>
          A contaminação cruzada ocorre quando há transferência direta ou indireta de contaminantes físicos, químicos ou biológicos de um alimento, utensílio, vetor ou manipulador para alimentos que serão consumidos.Pode ocorrer nas diferentes etapas do processo de produção do alimento: pré-preparo, tratamento, armazenamento, transporte, serviço.{"\n"}{"\n"}
          São fontes de contaminação{"\n"}
          •	Esponjas{"\n"}
          •	Panos de prato{"\n"}
          •	Colher de pau{"\n"}
          •	Óleo para fritura, dentre outros.{"\n"}</Text>
          </ScrollView>
      </View>
      <StatusBar style="auto" />
    </View >

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',
    alignItems: 'center',
    height: '100%',
    width: '100%'
    // justifyContent: 'center',
  },
  img: {
    alignItems: 'center',
    justifyContent: 'center',
    width: '120%',
    paddingRight: '15%'
  },
  subtitulo: {
    color: '#fff'
  },
  text: {
    color: '#fff',
    paddingLeft: '5%'
  },
  texttitulo: {
    color: '#b2daaa',
    fontWeight: 'bold',
    paddingLeft: '5%'
  },
  header: {
    color: '#fff',
    fontSize: 40,
    fontFamily: 'sans-serif-medium',
    fontWeight: 'bold',
    backgroundColor: 'rgba(52, 52, 52, 0)'
  },
  backgroundImage: {
    flex: 2,
    alignItems: 'center',
    width: '100%'
  }
});
