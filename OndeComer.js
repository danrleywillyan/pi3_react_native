
import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Image, ImageBackground, ScrollView } from 'react-native';
import logo from './assets/sem_gluten.svg';
export default function OndeComer({ navigation }) {
  return (
    <View style={[styles.container, { flexDirection: "column" }]}>
      <ImageBackground source={require('./assets/sem_gluten.png')} resizeMode="cover" style={styles.backgroundImage}>
      </ImageBackground>
      <View style={{ flex: 5, width: '100%', flexDirection: 'column', alignItems: 'center' }} >
        <ScrollView>
          <View >
          <Image style={styles.img} source={require('./assets/page_ondecomer.png')} />
          </View>
          <Text style={styles.texttitulo}>{"\n"}Incluir GeoLocalização com os endereços de restaurantes sem gluten{"\n"} </Text>
          </ScrollView>
      </View>
      <StatusBar style="auto" />
    </View >

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',
    alignItems: 'center',
    height: '100%',
    width: '100%'
    // justifyContent: 'center',
  },
  img: {
    alignItems: 'center',
    justifyContent: 'center',
    width: '120%',
    paddingRight: '15%'
  },
  subtitulo: {
    color: '#fff'
  },
  text: {
    color: '#fff',
    paddingLeft: '5%'
  },
  texttitulo: {
    color: '#b2daaa',
    fontWeight: 'bold',
    paddingLeft: '5%'
  },
  header: {
    color: '#fff',
    fontSize: 40,
    fontFamily: 'sans-serif-medium',
    fontWeight: 'bold',
    backgroundColor: 'rgba(52, 52, 52, 0)'
  },
  backgroundImage: {
    flex: 2,
    alignItems: 'center',
    width: '100%'
  }
});
