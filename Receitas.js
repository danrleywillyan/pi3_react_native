
import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Image, ImageBackground, ScrollView } from 'react-native';
import logo from './assets/sem_gluten.svg';
export default function Receitas({ navigation }) {
  return (
    <View style={[styles.container, { flexDirection: "column" }]}>
      <ImageBackground source={require('./assets/sem_gluten.png')} resizeMode="cover" style={styles.backgroundImage}>
      </ImageBackground>
      <View style={{ flex: 5, width: '100%', flexDirection: 'column', alignItems: 'center' }} >
        <ScrollView>
          <View >
          <Image style={styles.img} source={require('./assets/page_receitas.png')} />
          </View>
          <Text style={styles.texttitulo}>{"\n"}Pizza sem glúten light{"\n"} </Text>
          <Text style={styles.text}>{"\n"}
            TEMPO DE PREPARO 45 mins{"\n"}
            TEMPO DE ESPERA 15 mins{"\n"}
            REFEIÇÃO Jantar{"\n"}
            CULINÁRIA Brasileira{"\n"}
            RENDIMENTO 10{"\n"}
            CALORIAS 261 kcal{"\n"}
            EQUIPAMENTO{"\n"}
            •	Tigela grande{"\n"}
            •	Forma de pizza{"\n"}{"\n"}
            INGREDIENTES{"\n"}
            •	1½ xícaras polvilho doce{"\n"}
            •	1½ xícaras farinha de arroz{"\n"}
            •	2 colheres de chá açúcar{"\n"}
            •	1 colher de chá sal{"\n"}
            •	1 envelope fermento biológico seco (10 gramas){"\n"}
            •	2 ovo ligeiramente batidos{"\n"}
            •	½ xícara azeite de oliva{"\n"}
            •	½ xícara água morna{"\n"}{"\n"}
            INSTRUÇÕES{"\n"}
            1.	Misturar o polvilho doce, a farinha de arroz, o açúcar e o sal em uma tigela. Acrescentar o fermento e a água morna. Adicionar os ovos e o azeite. Misturar bem com uma colher.{"\n"}
            2.	Com as mãos, misturar tudo mais uma vez para incorporar bem os ingredientes. Porém, como a massa não tem glúten, não precisa sovar.{"\n"}
            3.	Abrir a massa com o auxílio das mãos em cima de uma forma de pizza. Deixar a massa descansar por 30 minutos. Pouco antes de dar o tempo, preaquecer o forno em 200º C (médio-alto).{"\n"}
            4.	Passados os 30 minutos, levar a massa ao forno preaquecido para assar por 10 minutos.{"\n"}
            5.	Após, retirar do forno, rechear a pizza do jeito que desejar e levar novamente ao forno para aquecer um pouco. Depois é só servir!{"\n"}
            {"\n"}{"\n"} </Text>
          <Text style={styles.texttitulo}>{"\n"}Pizza sem glúten{"\n"} </Text>
          <Text style={styles.text}>{"\n"}
            Receita{"\n"}
            •	1/2 xícara de farinha de arroz{"\n"}
            •	1/2 xícara de fécula de batata ou amido de milho{"\n"}
            •	1/2 copo de goma hidratada de tapioca- aquelas que compramos no mercado prontas para usar{"\n"}
            •	1 colher de chá de goma xantana{"\n"}
            •	1 colher de chá de sal{"\n"}
            •	1/2 colher de sopa de fermento biológico seco{"\n"}
            •	1 colher de chá de açúcar demerara{"\n"}
            •	1/2 copo de água morna{"\n"}
            •	1 colher de sopa de azeite de oliva{"\n"}
            •	1 ovo{"\n"}
            Passo a passo Massa de pizza sem glúten:{"\n"}
            1.	Primeiro de tudo vamos fazer a pré-fermentação! Dissolva o fermento e o açúcar na água morna – a temperatura ideal é quando você coloca o dedo e está confortável, espere o fermento reagir (cerca de 15 minutos).{"\n"}
            2.	Em uma tigela misture a farinha de arroz, a fécula de batata, goma de tapioca e a goma xantana e reserve. Faça um buraco no meio da mistura dos secos e adicione a mistura do fermento e o ovo levemente batido e misture até obter uma massa lisa. Coloque o óleo e o sal por último e misture bem. A massa fica bem grudenta, não tem como manipular com a mão.{"\n"}
            3.	Deixe a massa descansando em ambiente quente até dobrar de volume (eu deixo a minha perto do forno e abafo com um pano). Abra a massa em formato de pizza, eu coloco na forma antiaderente e espalhe com um espátula mesmo. Leve ao forno pré-aquecido a 180 graus por 10 a 15 minutos. Tire do forno, coloque o recheio de sua preferência e leve novamente para assar por mais uns 15 minutos a 200 graus C.{"\n"}
            {"\n"} </Text>
          <Text style={styles.texttitulo}>{"\n"}Massa de Panqueca sem Glúten{"\n"} </Text>
          <Text style={styles.text}>{"\n"}
            {"\n"}
            Ingredientes {"\n"}
            {"\n"}
            •	1 ovo{"\n"}
            •	1/2 xícara (de chá) de leite{"\n"}
            •	1 colher (de chá) de fermento de bolo{"\n"}
            •	1 xícara (de chá) de amido de milho (maisena){"\n"}
            •	1/2 colher (de chá) de sal (se o recheio for salgado substitua por 1 cubo de caldo de galinha sem glúten){"\n"}
            Modo de Preparo {"\n"}
            {"\n"}
            1.	Coloque os ingredientes no liquidificador e bata bem para misturar.{"\n"}
            2.	Aqueça uma frigideira untada com óleo. {"\n"}
            3.	Despeje uma concha de massa na frigideira aquecida e vá virando até o líquido forrar todo o fundo da frigideira. Aqui tem um segredo: Coloque pouco líquido e vá espalhando, quanto mais fininha ela fica, mais gostosa.{"\n"}
            4.	Deixe secar a massa como na foto 3 e com a ajuda de uma espátula reta, vire e deixe dourar um pouco do outro lado também. {"\n"}
            5.	Vá colocando uma sobre a outra em um prato. Logo que a panqueca sai do fogo ela fica meio quebradiça, deixe ela descansando um pouco e só depois recheie e dobre.{"\n"}
            {"\n"} </Text>
          <Text style={styles.texttitulo}>{"\n"}Cuca de goiaba sem glúten{"\n"} </Text>
          <Text style={styles.text}>{"\n"}
            Ingredientes{"\n"}
            •	2 OVOS{"\n"}
            •	2 xícaras de FARINHA SEM GLÚTEN{"\n"}
            •	½ xícara de ÓLEO VEGETAL{"\n"}
            •	1 ¼ xícara de AÇÚCAR{"\n"}
            •	½ xícara de LEITE VEGETAL{"\n"}
            •	2 colheres de chá de EXTRATO DE BAUNILHA{"\n"}
            •	2 colheres de chá de FERMENTO EM PÓ{"\n"}
            •	400g de GOIABADA CREMOSA{"\n"}
            •	1 xícara de FARINHA SEM GLÚTEN{"\n"}
            •	1/2 xícara de AÇÚCAR{"\n"}
            •	4 colheres de sopa ÓLEO DE COCO{"\n"}
            •	1 pitada de SAL{"\n"}
            •	1 colher de chá de EXTRATO DE BAUNILHA{"\n"}
            Modo de Preparo{"\n"}
            {"\n"}
            1.	Amasse todos os ingredientes com os dedos até formar uma farofa. Reserve.Pré-aqueça o forno a 170˚C.{"\n"}
            2.	Forre uma assadeira retangular média com papel manteiga.{"\n"}
            3.	Bata na batedeira na velocidade média os ovos e o açúcar por 6 minutos, até ficar espesso e esbranquiçado. Adicione o óleo e bata até incorporar.{"\n"}
            4.	Incorpore a farinha, o leite, a baunilha e o fermento.{"\n"}
            5.	Espalhe a massa na assadeira e coloque colheradas da goiabada por cima da massa.{"\n"}
            6.	Cubra com a farofa e asse por cerca de 1h, até o teste do palito sair seco.{"\n"} </Text>
          <Text style={styles.texttitulo}>{"\n"}Empadinha sem glúten{"\n"} </Text>
          <Text style={styles.text}>{"\n"}
            Ingredientes {"\n"}
            •	1 xícara de farinha de arroz – de preferência integral{"\n"}
            •	1 xícara de farinha de grão de bico ou de amaranto – caso não tenha pode ser mais 1 de arroz{"\n"}
            •	4 colheres de sopa de polvilho doce{"\n"}
            •	2 colheres de sopa de farinha de linhaça ou 1 de semente de chia – opcional, para agregar nutrientes{"\n"}
            •	2 colheres de chá rasas de sal{"\n"}
            •	4 colheres de sopa de manteiga ghee ou azeite de oliva{"\n"}
            •	2 ovos{"\n"}
            •	água gelada{"\n"}
            •	1 gema para pincelar – opcional{"\n"}
            Passo a passo {"\n"}
            1.	Misture as farinhas e o sal em uma tigela. Adicione a manteiga ghee e misture bem para que a maior parte das farinhas entrem em contato com a manteiga – eu misturo com uma colher mesmo – fica como se fosse uma farofa.{"\n"}
            2.	Bata levemente o ovo e adicione à mistura das farinhas e ghee, incorporando bem.{"\n"}
            3.	Se necessário adicione água gelada até dar o ponto – que deve ser de uma massa firme. Vá adicionando com uma colher, a quantidade normalmente não é grande, assim fica mais fácil de controlar e não perder o ponto.{"\n"}
            4.	Faça uma bolinha com a massa, enrole em papel filme e leve à geladeira por 30 minutos.{"\n"}
            5.	Enquanto isso, faça seu recheio. O meu eu fiz de legumes com tofu, mas fica a seu critério. Pode fazer com frango desfiado, camarão, siri, cogumelos, etc.{"\n"} </Text>
          <Text style={styles.texttitulo}>{"\n"}Bolo de cenoura sem glúten{"\n"} </Text>
          <Text style={styles.text}>{"\n"}
            {"\n"}
            Ingredientes {"\n"}
            •	1 xícara de farinha de arroz{"\n"}
            •	1 xícara de polvilho doce{"\n"}
            •	1 xícara de óleo{"\n"}
            •	1 xícara de açúcar (usei o cristal, mas pode ser demerara, só vai ficar mais escurinho){"\n"}
            •	2 cenouras descascadas e picadas{"\n"}
            •	1 e 1/2  colher de sopa de fermento em pó{"\n"}
            •	1 pitada de sal{"\n"}
            •	3 ovos{"\n"}
            •	100g de chocolate em barra (usei 70% cacau){"\n"}
            •	3/4 de xícara de leite{"\n"}
            Passo a passo {"\n"}
            1.	Bata no liquidificador primeiro as cenouras com o óleo até virar um purêzinho {"\n"}
            2.	Depois bata os ovos e o açúcar e bata mais um pouco{"\n"}
            3.	Bata a farinha, o polvilho e o sal junto da mistura anterior{"\n"}
            4.	Por último misture o fermento sem bater, com uma colher mesmo{"\n"}
            5.	Transfira para fôrma untada e enfarinhada e leve ao forno preaquecido a 180 graus até passar no teste do palito{"\n"}
            6.	Leve o chocolate picado com o leite ao fogo baixinho e mexa até virar uma calda espessa e brilhante{"\n"}
            7.	Cubra o bolo imediatamente{"\n"}
            {"\n"} </Text>
          <Text style={styles.texttitulo}>{"\n"}Bolo de banana sem glúten de liquidificador{"\n"} </Text>
          <Text style={styles.text}>{"\n"}
            Ingredientes {"\n"}
            •	2 xícaras de farinha de arroz{"\n"}
            •	1 xícara de açúcar mascavo ou demerara{"\n"}
            •	3 bananas prata bem maduras{"\n"}
            •	1 colher de chá de canela em pó{"\n"}
            •	½ xícara de chá de leite{"\n"}
            •	½ xícara de chá de óleo{"\n"}
            •	3 ovos{"\n"}
            •	1 colher de sopa de fermento em pó{"\n"}
            •	1 pitadinha de sal{"\n"}
            Passo a passo {"\n"}
            {"\n"}
            1.	Bata no liquidificador os ovos com o açúcar, as bananas, o leite e o óleo.{"\n"}
            2.	Acrescente então os ingredientes secos – menos o fermento.{"\n"}
            3.	Misture o fermento utilizando a função "pulsar" algumas vezes.{"\n"}
            4.	Finalize transferindo sua massa para uma fôrma untada e enfarinhada.{"\n"}
            5.	Enfim, leve ao forno pré-aquecido a 180 graus por 30 minutos ou até passar no teste do palito{"\n"}
            {"\n"} </Text>
          <Text style={styles.texttitulo}>{"\n"}Bolo Prestígio Sem Glúten{"\n"} </Text>
          <Text style={styles.text}>{"\n"}
            {"\n"}
            Ingredientes do Bolo Prestígio Sem Glúten{"\n"}
            •	2 colheres (de sopa) de sementes de linhaça{"\n"}
            •	6 colheres (de sopa) de água{"\n"}
            •	1 e 1/2 xícara (de chá) de farinha de arroz branca{"\n"}
            •	1 xícara (de chá) de açúcar demerara{"\n"}
            •	1/2 xícara (de chá) de cacau em pó{"\n"}
            •	1 pitada de sal{"\n"}
            •	1/2 xícara (de chá) de chá de óleo{"\n"}
            •	1 xícara (de chá) de água quente{"\n"}
            •	1 colher (de sopa) de vinagre branco{"\n"}
            •	1 colher (de sopa) de fermento químico{"\n"}
            •	Beijinho:{"\n"}
            •	150g de chocolate meio amargo sem glúten e sem leite{"\n"}
            •	2 colheres (de sopa) de leite de coco de vidro{"\n"}
            Modo de Preparo {"\n"}
            1.	Pré-aqueça o forno a 210ºC. Em um liquidificador, coloque as sementes de linhaça e bata para triturar bem.{"\n"}
            2.	Coloque em uma vasilha e adicione as 6 colheres de água e deixe de molho por 15 minutos, até virar uma geleia.{"\n"}
            3.	Esta mistura serve para substituir o ovo.{"\n"}
            4.	Unte uma assadeira pequena com óleo e polvilhe cacau em pó.{"\n"}
            5.	Você pode usar farinha de arroz, porém a farinha deixará marcas brancas no seu bolo.{"\n"}
            6.	Em uma vasilha, coloque a farinha de arroz, o açúcar, o cacau, a pitada de sal, o óleo, e adicione a mistura de linhaça, a água quente, o vinagre e misture bem.{"\n"}
            7.	Adicione o fermento e misture novamente.{"\n"}
            8.	Coloque a massa na assadeira e leve para assar por cerca de 40 minutos ou até espetar um garfo ou uma faca e ele sair limpo.{"\n"}
            9.	Evite assar demais, seu bolo pode ficar seco e duro.{"\n"}
            10.	Deixe esfriar por pelo menos 15 minutos antes de desenformar. Reserve.{"\n"}
            11.	Faça o recheio; eu usei como base esta receita de beijinho, porém usei amido de milho no lugar da farinha de trigo, para o beijinho também ficar sem glúten.{"\n"}
            12.	Desligue o beijinho antes dele ter o ponto de enrolar, senão o recheio ficará muito duro, o meu ficou cerca de 15 minutos no fogo baixo. Deixe esfriar.{"\n"}
            13.	Corte o bolo ao meio, com uma faca ou com um barbante.{"\n"}
            14.	Coloque a parte de baixo no prato em que ele será montado, e molhe com algumas colheres de leite vegetal, nesta receita usei o de amêndoas (molhei com cerca de 4 colheres).{"\n"}
            15.	Coloque o beijinho e espalhe bem.{"\n"}
            16.	Tampe com a outra parte do bolo, faça alguns furos com o garfo e molhe com mais algumas colheres de leite vegetal. Ajeite o beijinho que estiver escorrendo, para ficar certinho na hora de cobrir.{"\n"}
            17.	Reserve.{"\n"}
            18.	Derreta 150g de chocolate meio amargo sem leite e espere esfriar.{"\n"}
            19.	Adicione o leite de coco e misture bem.{"\n"}
            20.	Cubra o bolo com esta mistura e salpique raspas de chocolate e coco ralado.{"\n"}
            21.	Este bolo é mais gostoso geladinho.{"\n"}
            22.	Recomendo deixar por ao menos umas 3 horas na geladeira antes de servir.{"\n"}
            23.	Está pronto!{"\n"}
            {"\n"}
            Fontes:{"\n"}
            https://www.mundoboaforma.com.br/polvilho-tem-carboidrato-e-gluten-tipos-variacoes-e-dicas/{"\n"}
            {"\n"}
            https://guiadaculinaria.com/receitas-sem-gluten/{"\n"}
            {"\n"}
          </Text>
          </ScrollView>
      </View>
      <StatusBar style="auto" />
    </View >

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',
    alignItems: 'center',
    height: '100%',
    width: '100%'
    // justifyContent: 'center',
  },
  img: {
    alignItems: 'center',
    justifyContent: 'center',
    width: '120%',
    paddingRight: '15%'
  },
  subtitulo: {
    color: '#fff'
  },
  text: {
    color: '#fff',
    paddingLeft: '5%'
  },
  texttitulo: {
    color: '#b2daaa',
    fontWeight: 'bold',
    paddingLeft: '5%'
  },
  header: {
    color: '#fff',
    fontSize: 40,
    fontFamily: 'sans-serif-medium',
    fontWeight: 'bold',
    backgroundColor: 'rgba(52, 52, 52, 0)'
  },
  backgroundImage: {
    flex: 2,
    alignItems: 'center',
    width: '100%'
  }
});
