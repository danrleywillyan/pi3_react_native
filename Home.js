
import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Image, ImageBackground, TouchableOpacity, Button } from 'react-native';
import logo from './assets/sem_gluten.svg';

//navigation
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

//Ativar os Icones
import Icon from "react-native-vector-icons/AntDesign" //https://oblador.github.io/react-native-vector-icons/

const Stack = createNativeStackNavigator();

export default function Home({ navigation }) {
  return (
    <View style={[styles.container, {
      // Try setting `flexDirection` to `"row"`.
      flexDirection: "column",

    }]}>
      <ImageBackground source={require('./assets/brasilia.png')} resizeMode="cover" style={styles.backgroundImage}>
      </ImageBackground>
      <View style={{ flex: 2, width: '100%', flexDirection: 'row', alignItems: 'center' }} >
        <View style={{flex:1, padding:5}}> 
          <Icon style={styles.icons} onPress={() => navigation.navigate('Oquee')} name="questioncircleo" size={40}  color="#b2daaa"/>
          <TouchableOpacity onPress={() => navigation.navigate('Oquee')} style={styles.appButtonContainer}>
            <Text style={styles.appButtonText}>O que é</Text>
          </TouchableOpacity>
          <Icon style={styles.icons} onPress={() => navigation.navigate('Diagnosticos')} name="hearto" size={40}  color="#b2daaa"/>
          <TouchableOpacity onPress={() => navigation.navigate('Diagnosticos')} style={styles.appButtonContainer}>
            <Text style={styles.appButtonText}>Diagnosticos</Text>
          </TouchableOpacity>
          <Icon style={styles.icons} onPress={() => navigation.navigate('Tratamento')} name="pay-circle-o1" size={40}  color="#b2daaa"/>
          <TouchableOpacity onPress={() => navigation.navigate('Tratamento')} style={styles.appButtonContainer}>
            <Text style={styles.appButtonText}>Tratamentos</Text>
          </TouchableOpacity>
        </View>
        <View style={{flex:1, padding:5}}>
          <Icon style={styles.icons} onPress={() => navigation.navigate('OndeComer')} name="shoppingcart" size={40}  color="#b2daaa"/>    
          <TouchableOpacity onPress={() => navigation.navigate('OndeComer')} style={styles.appButtonContainer}>
            <Text style={styles.appButtonText}>Onde comer?</Text>
          </TouchableOpacity>
          <Icon style={styles.icons} onPress={() => navigation.navigate('Receitas')} name="exception1" size={40}  color="#b2daaa"/>
          <TouchableOpacity onPress={() => navigation.navigate('Receitas')} style={styles.appButtonContainer}>
            <Text style={styles.appButtonText}>Receitas</Text>
          </TouchableOpacity>
          <Icon style={styles.icons} onPress={() => navigation.navigate('GrupoEAssociacoes')} name="team" size={40}  color="#b2daaa"/>
          <TouchableOpacity onPress={() => navigation.navigate('GrupoEAssociacoes')} style={styles.appButtonContainer}>
            <Text style={styles.appButtonText}>Grupos e Associações</Text>
          </TouchableOpacity>
        </View> 
      </View>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',
    alignItems: 'center',
    height: '100%',
    width: '100%'
  },
  img: {
    alignItems: 'center',
    justifyContent: 'center',
    height: '60%'
  },
  text: {
    color: '#fff'
  },
  header: {
    color: '#fff',
    fontSize: 40,
    fontFamily: 'sans-serif-medium',
    fontWeight: 'bold',
    backgroundColor: 'rgba(52, 52, 52, 0)'
  },
  backgroundImage: {
    flex: 3,
    alignItems: 'center',
    width: '100%',
  },
  box:{
    flexDirection: "row",
    flex: 1,
    maxHeight: "50%",
    color: '#fff', 
    fontSize: 20, 
    width: '50%', 
    paddingLeft: '10%',

  },
  icons: {
    textAlign: "center",
    paddingTop: 7,
    paddingLeft: 5,
  },
  appButtonContainer: {
    backgroundColor: "transparent",
  },
  appButtonText: {
    fontSize: 14,
    color: "#fff",
    alignSelf: "center",
  }
});
