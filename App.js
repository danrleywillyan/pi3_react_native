import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Image, ImageBackground } from 'react-native';
import logo from './assets/sem_gluten.svg';

//navigation
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

//pages
import Oquee from './Oquee';
import Home from './Home';
import Diagnosticos from './Diagnostico';
import GrupoEAssociacoes from './GrupoEAssociacoes';
import OndeComer from './OndeComer';
import Receitas from './Receitas';
import Tratamento from './Tratamento';

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="Oquee" component={Oquee} />
        <Stack.Screen name="OndeComer" component={OndeComer} />
        <Stack.Screen name="Diagnosticos" component={Diagnosticos} />
        <Stack.Screen name="GrupoEAssociacoes" component={GrupoEAssociacoes} />
        <Stack.Screen name="Receitas" component={Receitas} />
        <Stack.Screen name="Tratamento" component={Tratamento} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}