
import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Image, ImageBackground, ScrollView } from 'react-native';
import logo from './assets/sem_gluten.svg';
export default function Oquee({ navigation }) {
  return (
    <View style={[styles.container, { flexDirection: "column" }]}>
      <ImageBackground source={require('./assets/sem_gluten.png')} resizeMode="cover" style={styles.backgroundImage}>
      </ImageBackground>
      <View style={{ flex: 5, width: '100%', flexDirection: 'column', alignItems: 'center' }} >
        <ScrollView>
          <View >
          <Image style={styles.img} source={require('./assets/page_oquee.png')} />
          </View>
            <Text style={styles.text}>{"\n"}Doença celíaca é uma doença autoimune, crônica e multiorgânica que afeta o intestino delgado em pessoas geneticamente predispostas devido à ingestão de glúten, uma proteína encontrada no trigo, aveia, cevada, centeio e seus derivados, como massas, pizzas, bolos, pães, biscoitos, cerveja, uísque, vodka e alguns doces.</Text>
            <Text style={styles.texttitulo}>{"\n"}Quantitativo de pessoas com Doença Celíaca</Text>
            <Text style={styles.text}>{"\n"}Estima-se que 1% da população mundial tenha doença celíaca, inclusive levando a óbito cerca de 42 mil crianças por ano.{"\n"}
              •	Afeta em torno de 2 milhões de pessoas no Brasil, mas a maioria delas encontra-se sem diagnóstico.{"\n"}
              •	Na Europa a prevalência oscila entre 1: 150 e 1: 300.{"\n"}
              •	Os estudos amostrais realizados em São Paulo, Ribeirão Preto e Brasília permitem estimar a incidência da doença em 1: 214, 1: 273 e 1: 681, respectivamente.Esta constatação coloca o Brasil ao nível da população europeia - a mais afetada.{"\n"}
              •	A doença celíaca pode aparecer em qualquer fase da vida, e atualmente, estima-se que a cada 400 brasileiros um seja celíaco.{"\n"}
              •	De cada oito pessoas que possuem a doença, apenas uma tem o diagnóstico.{"\n"}
              •	A doença celíaca é cosmopolita e afeta pessoas de todas as classes sociais.No Brasil a miscigenação vem rompendo a barreira etno-racial sendo diagnosticada entre os afrodescendentes e os povos indígenas.{"\n"}</Text>
            <Text style={styles.texttitulo}>{"\n"}Como o corpo reage a essa intolerância?</Text>
            <Text style={styles.text}>{"\n"}O corpo de quem tem o problema não possui uma enzima responsável por quebrar o glúten.Como a proteína não é processada direito, o sistema imune reage ao acúmulo e ataca a mucosa do intestino delgado causando um processo inflamatório crônico e prejudicando a absorção de nutrientes.{"\n"}</Text>
            <Text style={styles.texttitulo}>{"\n"}Genética</Text>
            <Text style={styles.text}>{"\n"}Predisposição genética: familiares de pacientes celíacos têm maior risco de desenvolver o quadro.</Text>
            <Text style={styles.texttitulo}>{"\n"}Sintomas</Text>
            <Text style={styles.text}>{"\n"}Os sintomas, em geral, aparecem entre os seis meses e dois anos e meio de vida.No entanto, isso não é regra.Portadores da doença podem manifestar os sintomas na fase adulta: {"\n"}
              •	Barriga estufada{"\n"}
              •	Danos à parede intestinal{"\n"}
              •	Gases{"\n"}
              •	Dor abdominal{"\n"}
              •	Ânsia de vômito{"\n"}
              •	Falta de apetite{"\n"}
              •	Diarreia ou prisão de ventre crônica{"\n"}
              •	Baixa absorção de nutrientes{"\n"}
              •	Irritabilidade{"\n"}
              •	Osteoporose{"\n"}
              •	Anemia{"\n"}
              •	Perda de peso{"\n"}
              •	Lesões na pele{"\n"}
              •	Queda de cabelo{"\n"}
              •	Dermatites{"\n"}
              •	Sintomas neurológicos{"\n"}</Text>
              </ScrollView>
      </View>
      <StatusBar style="auto" />
    </View >

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',
    alignItems: 'center',
    height: '100%',
    width: '100%'
    // justifyContent: 'center',
  },
  img: {
    alignItems: 'center',
    justifyContent: 'center',
    width: '120%',
    paddingRight: '15%'
  },
  subtitulo: {
    color: '#fff'
  },
  text: {
    color: '#fff',
    paddingLeft: '5%'
  },
  texttitulo: {
    color: '#b2daaa',
    fontWeight: 'bold',
    paddingLeft: '5%'
  },
  header: {
    color: '#fff',
    fontSize: 40,
    fontFamily: 'sans-serif-medium',
    fontWeight: 'bold',
    backgroundColor: 'rgba(52, 52, 52, 0)'
  },
  backgroundImage: {
    flex: 2,
    alignItems: 'center',
    width: '100%'
  }
});
